export type VectorFeatureFileFormat = 'kml' | 'geojson' | 'gpx';
export type PrintFileFormat = 'svg' | 'pdf' | 'png';
export type ColorVariable = 'intensity' | 'natural' | 'classification' | 'uniform';
export type ColorPalette = 'spectral' | 'greys' | 'blues' | 'viridis' | 'magma';
