enum GeoEvents {
  CustomEventType = 'GeoGirafe.App',
  undoDraw = 'undoDraw',
  zoomToExtent = 'zoomToExtent'
}

export default GeoEvents;
